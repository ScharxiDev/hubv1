package me.scharxidev.hub.events;

import me.scharxidev.hub.commands.CMD_Buid;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class EVENT_BuildMode implements Listener {

    @EventHandler
    public void onBuild(final BlockPlaceEvent event) {
        if (!CMD_Buid.getBuild().contains(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        if (!CMD_Buid.getBuild().contains(event.getPlayer())) {
            event.setCancelled(true);
        }
    }

}
