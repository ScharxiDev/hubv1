package me.scharxidev.hub.events;

import me.scharxidev.hub.util.ItemBuilder;
import me.scharxidev.hub.util.LobbyInventory;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.List;

public class EVENT_PlayerHider implements Listener {

    public List<Player> hide = new ArrayList<>();
    public LobbyInventory lobbyInventory;

    @EventHandler
    @SuppressWarnings("deprecation")
    public void onInteract(final PlayerInteractEvent event) {
        if (event.getItem() != null) {
            lobbyInventory = new LobbyInventory(event.getPlayer());
            if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(lobbyInventory.getPlayerHider())) {
                hide.add(event.getPlayer());
                Bukkit.getOnlinePlayers().forEach(p -> {
                    if (event.getPlayer() != p) {
                        event.getPlayer().hidePlayer(p);
                        event.getPlayer().playEffect(event.getPlayer().getLocation(), Effect.ENDER_SIGNAL, 1);
                    }
                });
                event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.UI_BUTTON_CLICK, 3, 1);
                event.getPlayer().getInventory().setItem(2, new ItemBuilder(Material.STICK).setName("§a§lShow player §8(§7Rightclick§8)").setAmount(1).build());
                event.getPlayer().updateInventory();
            } else if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§a§lShow player §8(§7Rightclick§8)")) {
                hide.remove(event.getPlayer());
                Bukkit.getOnlinePlayers().forEach(p -> {
                    if (event.getPlayer() != p) {
                        event.getPlayer().showPlayer(p);
                        event.getPlayer().playEffect(event.getPlayer().getLocation(), Effect.ENDER_SIGNAL, 1);
                    }
                });
                event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.BLOCK_LEVER_CLICK, 3, 1);
                event.getPlayer().getInventory().setItem(2, new ItemBuilder(Material.BLAZE_ROD).setName(lobbyInventory.getPlayerHider()).setAmount(1).build());
                event.getPlayer().updateInventory();
            }
        }
    }

    @EventHandler
    @SuppressWarnings("deprecation")
    public void onJoin(final PlayerJoinEvent event) {
        Bukkit.getOnlinePlayers().forEach(p -> {
            if (hide.contains(p)) {
                if (p != event.getPlayer()) {
                    p.hidePlayer(event.getPlayer());
                }
            }
        });
    }

    @EventHandler
    public void onLeave(final PlayerQuitEvent event) {
        if (hide.contains(event.getPlayer())) {
            hide.remove(event.getPlayer());
        }
    }


}
