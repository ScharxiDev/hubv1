package me.scharxidev.hub.events;

import me.scharxidev.hub.Main;
import me.scharxidev.hub.util.LobbyInventory;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class EVENT_Join_Quit implements Listener {

    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        event.setJoinMessage(null);
        event.getPlayer().setHealth(20.0);
        event.getPlayer().setFoodLevel(20);
        new LobbyInventory(event.getPlayer()).setLobbyInventory();
        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP,3,1);
    }

    @EventHandler
    public void onQuit(final PlayerQuitEvent event) {
        event.setQuitMessage(null);
    }

}
