package me.scharxidev.hub.events;

import me.scharxidev.hub.Main;
import me.scharxidev.hub.util.ItemBuilder;
import me.scharxidev.hub.util.TeleportUtil;
import org.bukkit.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EVENT_Navigator implements Listener {

    public final HashMap<String, Location> locationHashmap = new HashMap<>();


    @EventHandler
    public void onClick(final PlayerInteractEvent event) {
        if (event.getItem() != null) {
            if (event.getItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', Main.getFmanager().getConfig().getString("Lobby Items.Prefixes.Navigator")))) {
                Inventory inventory = Bukkit.createInventory(null, 36, ChatColor.translateAlternateColorCodes('&', Main.getFmanager().getConfig().getString("Inventory.Prefixes.Navigator")));

                List<String> list = getWarp();

                for (String s : list) {
                    try {
                        String name = s.split(":")[0];
                        Material material = Material.valueOf(s.split(":")[1].toUpperCase());
                        int slot = Integer.parseInt(s.split(":")[2]);
                        Location loc = new Location(Bukkit.getWorld(s.split(":")[3]), Double.parseDouble(s.split(":")[4]), Double.parseDouble(s.split(":")[5]), Double.parseDouble(s.split(":")[6]), Float.parseFloat(s.split(":")[7]), Float.parseFloat(s.split(":")[8]));

                        locationHashmap.put(name, loc);
                        inventory.setItem(slot, new ItemBuilder(material).setName(name).build());
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                }

                event.getPlayer().openInventory(inventory);
            }
        }
    }

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        if (event.getClickedInventory().getName().equals(ChatColor.translateAlternateColorCodes('&', Main.getFmanager().getConfig().getString("Inventory.Prefixes.Navigator")))) {
            if (event.getCurrentItem() != null) {
                if (event.getCurrentItem().getItemMeta().hasDisplayName()) {
                    String displayName = event.getCurrentItem().getItemMeta().getDisplayName();
                    if (locationHashmap.containsKey(displayName)) {
                        event.getWhoClicked().closeInventory();
                        event.getWhoClicked().teleport(locationHashmap.get(displayName));
                        event.getWhoClicked().sendMessage(Main.getPrefix() + "§aWarped to §e" + displayName);
                    }
                }
            }
            event.setCancelled(true);
        }
    }

    public static List getWarp() {
        try {
            return TeleportUtil.getConfiguration().getStringList("warps");
        } catch (Exception e) {}
        return new ArrayList<>();
    }

}
