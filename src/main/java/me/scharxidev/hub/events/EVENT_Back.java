package me.scharxidev.hub.events;

import me.scharxidev.hub.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class EVENT_Back implements Listener {

    @EventHandler
    public void onDeath(final PlayerDeathEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        event.setKeepInventory(true);
        Main.back.put(event.getEntity().getPlayer(), event.getEntity().getLocation());
    }

}
