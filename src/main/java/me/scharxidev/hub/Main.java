package me.scharxidev.hub;

import me.scharxidev.hub.commands.*;
import me.scharxidev.hub.events.*;
import me.scharxidev.hub.util.FileManager;
import me.scharxidev.hub.util.TeleportUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public final class Main extends JavaPlugin {

    private static Main instance;
    private static String prefix;
    public static FileManager fmanager;
    private static String noPerm;
    private static String asPlayer;
    private static TeleportUtil teleportUtil;
    private PluginManager manager;
    public final static HashMap<Player, Location> back = new HashMap<>();


    @Override
    public void onEnable() {
        instance = this; // Initial instance
        manager = Bukkit.getPluginManager(); // Initial manager
        fmanager = new FileManager(getInstance());
        saveDefaultConfig();
        teleportUtil = new TeleportUtil(this);
        this.initCommands();
        this.initEvents();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private void initCommands() {
        this.initializeCommands("build", new CMD_Buid());
        this.initializeCommands("vanish", new CMD_Vanish());
        this.initializeCommands("reloadconfig", new CMD_ReloadConfig());
        this.initializeCommands("setwarp", new CMD_SetWarp());
        this.initializeCommands("warps", new CMD_Warps());
        this.initializeCommands("gm", new CMD_Gamemode());
        this.initializeCommands("back", new CMD_Back());
    }

    private void initEvents() {
        this.initializeEvents(new EVENT_Join_Quit());
        this.initializeEvents(new EVENT_Default());
        this.initializeEvents(new EVENT_Inventory());
        this.initializeEvents(new EVENT_BuildMode());
        this.initializeEvents(new EVENT_Navigator());
        this.initializeEvents(new EVENT_PlayerHider());
        this.initializeEvents(new EVENT_Back());

    }

    private void initializeEvents(Listener listener) {
        getManager().registerEvents(listener, this);
    }

    private void initializeCommands(String command, CommandExecutor executor) {
        getCommand(command).setExecutor(executor);
    }

    public PluginManager getManager() {
        return manager;
    }

    public static Main getInstance() {
        return instance;
    }

    public static String getAsPlayer() {
        Main.asPlayer = ChatColor.translateAlternateColorCodes('&', fmanager.getConfig().getString("Prefixes.As-Player"));
        return asPlayer;
    }

    public static String getNoPerm() {
        Main.noPerm = ChatColor.translateAlternateColorCodes('&', fmanager.getConfig().getString("Prefixes.No-Perissions"));
        return noPerm;
    }

    public static String getPrefix() {
        Main.prefix = ChatColor.translateAlternateColorCodes('&', fmanager.getConfig().getString("Prefixes.General"));
        return prefix;
    }

    public static TeleportUtil getTeleportUtil() {
        return teleportUtil;
    }

    public static FileManager getFmanager() {
        return fmanager;
    }
}
