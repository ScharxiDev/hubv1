package me.scharxidev.hub.util;

import me.scharxidev.hub.Main;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TeleportUtil {

    private static File file;
    private static YamlConfiguration configuration;

    public TeleportUtil(Main plugin) {
        file = new File(plugin.getDataFolder(), "warps.yml");
        if (!file.getParentFile().exists()) {
            try {
                file.getParentFile().mkdirs();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            configuration = YamlConfiguration.loadConfiguration(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void save(String name, Material material, int slot, Location loc) {
        String toSave = name + ":" +
                material.toString() + ":" +
                slot + ":" +
                loc.getWorld().getName() + ":" +
                loc.getX() + ":" +
                loc.getY() + ":" +
                loc.getZ() + ":" +
                loc.getYaw() + ":" +
                loc.getPitch();

        List<String> list = new ArrayList<>();

        try {
           list = configuration.getStringList("warps");
        } catch (Exception e) {
            list = new ArrayList<>();
        }

        list.add(toSave);
        configuration.set("warps", list);
        try {
            configuration.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static YamlConfiguration getConfiguration() {
        return configuration;
    }
}
