package me.scharxidev.hub.util;

import me.scharxidev.hub.Main;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;

public class FileManager {


    private Main plugin;
    private FileConfiguration configuration = null;
    private File configFile = null;


    public FileManager(Main plugin) {
        this.plugin = plugin;
        this.saveDefaultConfig(); // save/initialize config
    }

    public void reloadConfig() {
        if (!this.configFile.exists())
            this.configFile = new File(this.plugin.getDataFolder().getPath(), "config.yml");

        this.configuration = YamlConfiguration.loadConfiguration(configFile);

        InputStream defaultStream = this.plugin.getResource("config.yml");
        if (defaultStream != null) {
            YamlConfiguration defaultConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defaultStream));
            this.configuration.setDefaults(defaultConfig);
        }
    }

    public void saveConfig() {
        if (this.configuration == null || this.configFile == null)
            return;

        try {
            this.getConfig().save(configFile);
        } catch (IOException e) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + this.configFile, e);
        }
    }

    public void saveDefaultConfig() {
        if (this.configFile == null)
            this.configFile = new File(this.plugin.getDataFolder().getPath(), "config.yml");

        if (!this.configFile.exists()){
            this.plugin.saveResource("config.yml", false);
        }
        else{
            this.plugin.saveResource("config.yml", true);
        }

    }

    public FileConfiguration getConfig() {
        if (this.configuration == null)
            reloadConfig();

        return this.configuration;
    }


}
