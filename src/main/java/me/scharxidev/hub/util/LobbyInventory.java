package me.scharxidev.hub.util;

import me.scharxidev.hub.Main;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class LobbyInventory {

    Player p;
    String teleporterName;
    String playerHiderName;
    String noGadgetsName;
    String gadetName;
    String profile;

    private static LobbyInventory lobbyInventory;

    public LobbyInventory(Player p) {
        this.p = p;
        lobbyInventory = this;
        this.teleporterName = ChatColor.translateAlternateColorCodes('&',Main.fmanager.getConfig().getString("Lobby Items.Prefixes.Navigator"));
        this.playerHiderName = ChatColor.translateAlternateColorCodes('&',Main.fmanager.getConfig().getString("Lobby Items.Prefixes.Playerhider"));
        this.noGadgetsName = ChatColor.translateAlternateColorCodes('&',Main.fmanager.getConfig().getString("Lobby Items.Prefixes.No-Gadgets"));
        this.gadetName = ChatColor.translateAlternateColorCodes('&',Main.fmanager.getConfig().getString("Lobby Items.Prefixes.Gadgets"));
        this.profile = ChatColor.translateAlternateColorCodes('&',Main.fmanager.getConfig().getString("Lobby Items.Prefixes.Profile"));
    }

    public void setLobbyInventory() {
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);
        p.getInventory().setItem(0, new ItemBuilder(Material.COMPASS).setName(this.teleporterName).setAmount(1).build());
        p.getInventory().setItem(2, new ItemBuilder(Material.BLAZE_ROD).setName(this.playerHiderName).setAmount(1).build());
        p.getInventory().setItem(4, new ItemBuilder(Material.BARRIER).setName(this.noGadgetsName).setAmount(1).build());
        p.getInventory().setItem(6, new ItemBuilder(Material.CHEST).setName(this.gadetName).setAmount(1).build());
        p.getInventory().setItem(8, new ItemBuilder(new ItemStack(Material.SKULL_ITEM, 1, (byte) 3)).setName(this.profile).setSkullOwner(p.getName()).buildSkull());
    }

    public String getPlayerHider() {
        return this.playerHiderName;
    }

}
