package me.scharxidev.hub.commands;

import me.scharxidev.hub.Main;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_Gamemode implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage(Main.getPrefix() + Main.getAsPlayer());
            return true;
        }

        if (!(sender.hasPermission("hub.changeGM"))) {
            sender.sendMessage(Main.getPrefix() + Main.getNoPerm());
            return true;
        }

        Player p = (Player) sender;

        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("0")) {
                if (p.getGameMode() == GameMode.SURVIVAL) return true;
                p.setGameMode(GameMode.SURVIVAL);
                p.sendMessage(Main.getPrefix() + "§aYou was set in to §esurvival mode.");
            } else if (args[0].equalsIgnoreCase("1")) {
                if (p.getGameMode() == GameMode.CREATIVE) return true;
                p.setGameMode(GameMode.CREATIVE);
                p.sendMessage(Main.getPrefix() + "§aYou was set in to §ecreative mode.");
            } else if (args[0].equalsIgnoreCase("2")) {
                if (p.getGameMode() == GameMode.ADVENTURE) return true;
                p.setGameMode(GameMode.ADVENTURE);
                p.sendMessage(Main.getPrefix() + "§aYou was set in to §eadventure mode.");
            } else if (args[0].equalsIgnoreCase("3")) {
                if (p.getGameMode() == GameMode.SPECTATOR) return true;
                p.setGameMode(GameMode.SPECTATOR);
                p.sendMessage(Main.getPrefix() + "§aYou was set in to §espectator mode.");
            }
        } else if (args.length == 2) {
            Player target = Bukkit.getPlayer(args[1]);

            if (target != null) {
                if (args[0].equalsIgnoreCase("0")) {
                    if (target.getGameMode() == GameMode.SURVIVAL) return true;
                    target.setGameMode(GameMode.SURVIVAL);
                    target.sendMessage(Main.getPrefix() + "§aYou was set in to §esurvival mode.");
                    p.sendMessage(Main.getPrefix() + "§e" + target.getName() + " §awas set in to §esurvival mode.");
                } else if (args[0].equalsIgnoreCase("1")) {
                    if (target.getGameMode() == GameMode.CREATIVE) return true;
                    target.setGameMode(GameMode.CREATIVE);
                    target.sendMessage(Main.getPrefix() + "§aYou was set in to §ecreative mode.");
                    p.sendMessage(Main.getPrefix() + "§e" + target.getName() + " §awas set in to §ecreative mode.");
                } else if (args[0].equalsIgnoreCase("2")) {
                    if (target.getGameMode() == GameMode.ADVENTURE) return true;
                    target.setGameMode(GameMode.ADVENTURE);
                    target.sendMessage(Main.getPrefix() + "§aYou was set in to §eadventure mode.");
                    p.sendMessage(Main.getPrefix() + "§e" + target.getName() + " §awas set in to §eadventure mode.");
                } else if (args[0].equalsIgnoreCase("3")) {
                    if (target.getGameMode() == GameMode.SPECTATOR) return true;
                    target.setGameMode(GameMode.SPECTATOR);
                    target.sendMessage(Main.getPrefix() + "§aYou was set in to §espectator mode.");
                    p.sendMessage(Main.getPrefix() + "§e" + target.getName() + " §awas set in to §espectator mode.");
                }
            } else {
                p.sendMessage(Main.getPrefix() + "§e" + args[1] + " §cis not online.");
                return true;
            }
        } else {
            sender.sendMessage(Main.getPrefix() + "§cToo much arguments!");
        }
        return true;
    }

}