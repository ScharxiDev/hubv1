package me.scharxidev.hub.commands;

import me.scharxidev.hub.Main;
import me.scharxidev.hub.util.TeleportUtil;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.List;

public class CMD_Warps implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0) {
            List<String> warps = TeleportUtil.getConfiguration().getStringList("warps");
            sender.sendMessage(Main.getPrefix() + "§aWarps: ");
            for (String s : warps) {
                String name = s.split(":")[0];
                sender.sendMessage(Main.getPrefix() + "§e" + name);
            }
        } else {
            sender.sendMessage(Main.getPrefix() + "§cToo much arguments!");
            sender.sendMessage(Main.getPrefix() + "§aUse: §e/warps");
        }
        return true;
    }
}