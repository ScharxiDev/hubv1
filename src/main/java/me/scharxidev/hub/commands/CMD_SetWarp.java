package me.scharxidev.hub.commands;

import me.scharxidev.hub.Main;
import me.scharxidev.hub.util.TeleportUtil;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_SetWarp implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender.hasPermission("hub.setup"))) {
            sender.sendMessage(Main.getPrefix() + Main.getNoPerm());
            return true;
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage(Main.getPrefix() + Main.getAsPlayer());
            return true;
        }

        Player p = (Player) sender;

        if (args.length == 3) {
            String name = args[0];
            Material material = null;
            int slot = 0;
            try {
                material = Material.valueOf(args[1].toUpperCase());
            } catch (IllegalArgumentException e) {
                sender.sendMessage(Main.getPrefix() + "§e" + args[1].toUpperCase() + " §cis not a valid material.");
                return true;
            }
            try {
                slot = Integer.parseInt(args[2]);
            } catch (NumberFormatException e) {
                sender.sendMessage(Main.getPrefix() + "§6" + args[2] + " §6must be a number.");
            }
            TeleportUtil.save(name, material, slot, p.getLocation());
            p.sendMessage(Main.getPrefix() + "§aSuccessfully created warp point -> §e" + name);
        } else {
            p.sendMessage(Main.getPrefix() + "§eUse: §a/setwarp <name> <material> <slot>");
        }
        return true;
    }

}