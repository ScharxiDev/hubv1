package me.scharxidev.hub.commands;

import me.scharxidev.hub.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class CMD_Vanish implements CommandExecutor {

    private static ArrayList<Player> vanishPlayers = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender.hasPermission("hub.vanish"))) {
            sender.sendMessage(Main.getNoPerm());
            return true;
        }

        if (!(sender instanceof Player)) {
            sender.sendMessage(Main.getAsPlayer());
            return true;
        }

        Player p = (Player) sender;

        if (args.length == 1) {
            Player target = Bukkit.getPlayer(args[0]);
            if (getVanishPlayers().contains(target)) {
                p.sendMessage("§e" + target.getName() + " §ais no longer in the vanish mode.");
                target.sendMessage("§cYou are no longer in vanish mode.");
                getVanishPlayers().remove(target);
                Bukkit.getOnlinePlayers().forEach(all -> {
                    all.showPlayer(target);
                });
            } else {
                getVanishPlayers().add(target);
                p.sendMessage("§e" + target.getName() + " §awas set in to the vanish mode.");
                target.sendMessage("§aYou was set in to the vanish mode.");
                Bukkit.getOnlinePlayers().forEach(all -> {
                    all.hidePlayer(p);
                });
            }
        } else if (args.length == 0) {
            if (getVanishPlayers().contains(p)) {
                getVanishPlayers().remove(p);
                p.sendMessage("§cYou are not longer in the vanish mode.");
                Bukkit.getOnlinePlayers().forEach(all -> {
                    all.showPlayer(p);
                });
            } else {
                getVanishPlayers().add(p);
                p.sendMessage("§aYou was set in to the vanish mode.");
                Bukkit.getOnlinePlayers().forEach(all -> {
                    all.hidePlayer(p);
                });
            }
        } else {
            sender.sendMessage("§eUse: §a/vanish §eor §a/vanish <player>");
        }


        return true;
    }

    public static ArrayList<Player> getVanishPlayers() {
        return vanishPlayers;
    }
}