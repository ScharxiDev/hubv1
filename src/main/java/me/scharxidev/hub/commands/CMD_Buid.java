package me.scharxidev.hub.commands;

import me.scharxidev.hub.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class CMD_Buid implements CommandExecutor {

    private static ArrayList<Player> build = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {


        if (!(sender instanceof Player)) {
            sender.sendMessage(Main.getPrefix() + Main.getAsPlayer());
            return true;
        }

        if (!(sender.hasPermission("hub.build"))) {
            sender.sendMessage(Main.getPrefix() + Main.getNoPerm());
            return true;
        }

        Player p = (Player) sender;

        if (args.length == 0) {
            if (getBuild().contains(p)) {
                getBuild().remove(p);
                p.sendMessage(Main.getPrefix() +"§cYou are no longer in the build mode.");
            } else {
                getBuild().add(p);
                p.sendMessage(Main.getPrefix() +"§aYou was set in to the build mode");
            }
        } else if (args.length == 1) {
            Player target = Bukkit.getPlayer(args[0]);
            if (getBuild().contains(target)) {
                getBuild().remove(target);
                p.sendMessage(Main.getPrefix() + "§e" + target.getName() + " §ais no longer in the build mode.");
                target.sendMessage(Main.getPrefix() + "§cYou are no longer in the build mode.");
            } else {
                getBuild().add(target);
                p.sendMessage(Main.getPrefix()  + "§e" + target.getName() + " §awas set in to the build mode.");
                target.sendMessage(Main.getPrefix() + "§aYou was set into the build mode.");
            }
        } else {
            sender.sendMessage(Main.getPrefix() + "§eUse: §a/build §eor §a/build <player>");
        }
        return true;
    }

    public static ArrayList<Player> getBuild() {
        return build;
    }
}