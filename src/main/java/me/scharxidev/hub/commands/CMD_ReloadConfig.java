package me.scharxidev.hub.commands;

import me.scharxidev.hub.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.logging.Level;

public class CMD_ReloadConfig implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender.hasPermission("hub.reloadConfig"))) {
            sender.sendMessage(Main.getPrefix() + Main.getNoPerm());
            return true;
        }

        if (args.length > 0) {
            sender.sendMessage(Main.getPrefix() + "§cToo much arguments.");
            return true;
        }

        if (args.length == 0) {
            Main.fmanager.reloadConfig();
            Main.getInstance().getLogger().log(Level.INFO, "Successfully reloaded config.");
        }

        return true;
    }


}