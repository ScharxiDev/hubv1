package me.scharxidev.hub.commands;

import me.scharxidev.hub.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMD_Back implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage(Main.getPrefix() + Main.getAsPlayer());
            return true;
        }
        if (args.length == 0) {
            Player p = (Player) sender;
            if (Main.back.containsKey(p)) {
                p.teleport(Main.back.get(p));
                Main.back.remove(p);
                p.sendMessage(Main.getPrefix() + "§aSuccessfully teleported to the last death location!");
            } else {
                p.sendMessage(Main.getPrefix() + "§cThere is no place to teleport.");
            }

        } else {
            sender.sendMessage(Main.getPrefix() + "§cToo much arguments!");
            sender.sendMessage(Main.getPrefix() + "§eUse: §a/back");
        }
        return true;
    }
}